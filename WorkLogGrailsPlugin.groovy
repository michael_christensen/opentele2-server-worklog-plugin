class WorkLogGrailsPlugin {
    // the plugin version
    def version = "0.1"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.4 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    def title = "Opentele work log plugin"
    def author = "OpenTele"
    def authorEmail = ""
    def description = '''\
OpenTele work log plugin contains functionality for extracting a textual summary
of recent patient actions and measurements.
'''

    // Extra (optional) plugin metadata
    def license = 'APACHE'

    def organization = [ name: "4S", url: "4s-online.dk" ]

    def developers = [ [ name: "OpenTeleHealth ApS", url: "OpenTeleHealth.com" ] ]

}
