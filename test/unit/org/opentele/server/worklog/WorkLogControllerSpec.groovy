package org.opentele.server.worklog

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.opentele.server.core.QuestionnaireService
import org.opentele.server.core.ResultTableViewModel
import org.opentele.server.model.Clinician
import org.opentele.server.model.Conference
import org.opentele.server.model.Consultation
import org.opentele.server.model.Department
import org.opentele.server.model.Message
import org.opentele.server.model.Patient
import org.opentele.server.model.PatientNote
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(WorkLogController)
@Build([Patient, Clinician])
class WorkLogControllerSpec extends Specification {

    Clinician clinician
    Patient patient

    def setup() {

        // Initialize test data
        patient = Patient.build()
        params.id = patient.id

        clinician = Clinician.build(firstName: 'Helle', lastName: 'Andersen')
        ResultTableViewModel result = new ResultTableViewModel()
        List<Message> messages = [ new Message(sendDate: new Date(),
                sentByPatient: false, department: new Department(name: "Some department"),
                title: "Message title", text: "This is a message") ]
        List<PatientNote> patientNotes = [ new PatientNote(note: "This is a note", createdDate: new Date()) ]

        // Mock domain objects
        mockDomain(Conference)
        mockDomain(Consultation)
        mockDomain(Message)

        // Mock controller methods
        controller.metaClass.currentClinician = { clinician }
        controller.metaClass.extractMessages = { Patient patient,
                                                 Date startDate,
                                                 Date endDate -> messages }
        controller.metaClass.extractNotes = { Patient patient,
                                              Date startDate,
                                              Date endDate -> patientNotes }
        controller.metaClass.message = { message ->
            if (message.code == 'workLog.header.dateFormat') {
                'dd/MM/yyyy'
            } else if (message.code == 'workLog.entry.dateFormat') {
                'dd/MM/yyyy, HH:mm:ss'
            } else {
                message.code
            }
        }

        // Mock questionnaireService
        def questionnaireServiceMock = Mock(QuestionnaireService)
        questionnaireServiceMock.extractCompletedQuestionnaireWithAnswers(patient.id, null, false, _) >> result
        controller.questionnaireService = questionnaireServiceMock

    }

    def "test search returns a string containing the relevant information"() {
        given:
        params.id = patient.id
        params.title = "Konsultation" //EEE MMM dd HH:mm:ss zzz yyyy
        params.startDate = new Date("2015/03/12")
        params.endDate = new Date("2015/04/13")
        params.locale = "da_DK"

        when:
        controller.search()

        then:
        model != null
        assert model.workLog.contains('Konsultation')
        assert model.workLog.contains('firstName')
        assert model.workLog.contains('lastName')
        assert model.workLog.contains('12/03/2015')
        assert model.workLog.contains('13/04/2015')
        assert model.workLog.contains('This is a note')
        assert model.workLog.contains('This is a message')
        assert model.workLog.contains('Some department')
        assert model.workLog.contains('Message title')

    }

}
